$(function () {

	const mapStyle = [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]




	$('.js_home__promo__move__mobile__wrap').bind('touchstart mousedown', function(e){
		$('.js_home__promo__move__mobile').fadeOut(250, function () {
			$(this).remove()
		});
	});

	$('.js_point_appartament_x__view').click(function(){
		if ($(this).closest('.point_appartament_x').hasClass('show')){
			$(this).closest('.point_appartament_x').removeClass('show')
			return false;
		}


		let $card = $(this).closest('.point_appartament_x').find('.point_appartament_x__card')
		let $cardView = $(this).closest('.point_appartament_x').find('.js_point_appartament_x__view')


		$('.image__appartment__block__inner .point_appartament_x').removeClass('show')
		$(this).closest('.point_appartament_x').toggleClass('show')


		if ($('.js_page_area__image_map').offset().top>$card.offset().top){
			$card.addClass('__onTop').css('top', $card.outerHeight() / 2)
		}

		if ($('.js_page_area__image_map').offset().top + $('.js_page_area__image_map').outerHeight()<$card.offset().top + $card.outerHeight()){
			$card.addClass('__onBottom').css('top', -1 * $card.outerHeight() / 2 + $cardView.outerHeight() + 20)
		}

	})


	$('.js_point_appartament_x__card__close').click(function(){
		$(this).closest('.point_appartament_x').removeClass('show');
	})







    function projectMap() {

        const centerCoord = {
            lat: parseFloat($('.js_project__location__map').attr('data-lat')),
            lng: parseFloat($('.js_project__location__map').attr('data-lng'))
        };

		const locations = [{
			lat: parseFloat($('.js_project__location__map').attr('data-lat')),
            lng: parseFloat($('.js_project__location__map').attr('data-lng'))
		}];


        const map = new google.maps.Map(document.querySelector(".js_project__location__map"), {
            center: centerCoord,
            styles :mapStyle,
            zoom:11
        });



		const markers = locations.map((item, i) => {
			const marker = new google.maps.Marker({
				position: {lat:item.lat, lng:item.lng},
				icon: '/images/icons/map_marker.svg',
			});

			return marker;
		  });

		  console.log(markers)


		  new markerClusterer.MarkerClusterer({ map, markers });
	}


    function filterMap() {
		const locations = []

        $('.js_map .marker').each(function () {
			locations.push({
				lat: parseFloat($(this).attr('data-lat')),
				lng: parseFloat($(this).attr('data-lng')),
				title: $(this).attr('data-title')
			})
		})


        const centerCoord = {
            lat: parseFloat($('.js_map').attr('data-lat')),
            lng: parseFloat($('.js_map').attr('data-lng'))
        };


        const map = new google.maps.Map(document.querySelector(".js_map"), {
            center: centerCoord,
            styles :mapStyle,
            zoom:11
        });


		const markers = locations.map((item, i) => {
			const marker = new google.maps.Marker({
				position: {lat:item.lat, lng:item.lng},
			  	title:item.title,
				icon: '/images/icons/map_marker.svg',
			});

			return marker;
		  });

		  new markerClusterer.MarkerClusterer({ map, markers });
	}


	function developMap($map){

		const locations = JSON.parse($map.attr('data-objects'));

        const centerCoord = {
            lat: locations[0].lat,
            lng: locations[0].lng,
        };


        const map = new google.maps.Map($map[0], {
            center: centerCoord,
            styles :mapStyle,
            zoom:11
        });

		let infowindow = new google.maps.InfoWindow();

		const markers = locations.map((item, i) => {


			let contentHtml = `
			<div class="point_appartament_x__card point_appartament_x__card--map">
				<a href="${item.link}" class="point_appartament_x__card__image">
					<div class="point_appartament_x__card__image__inner">
						<img src="${item.img}" alt="">
					</div>
				</a>
				<div class="point_appartament_x__card__info">
					<div class="point_appartament_x__card__title">
						<a href="${item.link}">${item.title}</a>
					</div>
					<div class="point_appartament_x__card__developer__text">${item.text}</div>
					<div class="point_appartament_x__card__developer__price">${item.price}</div>
					<a href="${item.link}" class="point_appartament_x__card__developer__more">
						${item.more_text}
						<svg width="6" height="9" viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1.25 8L4.75 4.5L1.25 1" stroke="#AA8B62" stroke-linecap="square"></path>
						</svg>
					</a>
				</div>
			</div>`;

			let markersGmap = [];
			const marker = new google.maps.Marker({
				position: {lat:item.lat, lng:item.lng},
				title: item.title,
				icon: '/images/icons/map_marker_default.svg',
				content: contentHtml
			});

			markersGmap.push(marker);

			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
				  infowindow.setContent(marker.content);
				  infowindow.open(map, marker);
				}
			  })(marker, i));


			return marker;
		  });

		  new markerClusterer.MarkerClusterer({ map, markers });

	}




    if ($('.js_page_developer_map').length) {
		$('.js_page_developer_map').each(function(){
			developMap($(this))
		})
	}

    if ($('.js_map').length) filterMap()
    if ($('.js_project__location__map').length) projectMap()



	$('.js_show_filter_map').click(function(){
		$(this).toggleClass('active')
		$('.js_filter__map').toggleClass('show')
	})




	$(window).on("load", function () {
		$('body').addClass('loaded')
	})

	$(".js_select").each(function (index, element) {
		let placeholder = $(this).attr('placeholder');
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			allowClear: true
		})
	});

	function validateEmail(email) {
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}

	function validateForm($form) {


		let validate = true;
		// for simple input
		$form.find('input[required][type="text"],textarea[required]').each(function () {
			if ($(this).val().length <= 0) {
				$(this).addClass('warning')
				validate = false;
			}
		})

		// for phone input
		$form.find('.js_phone').each(function () {
			if ($(this).val().slice(-1) == '_' || $(this).val().length <= 0) {
				$(this).addClass('warning')
				validate = false;
			}
		})

		// for email input
		$form.find('input[required][type="email"]').each(function () {
			if (!validateEmail($(this).val())) {
				$(this).addClass('warning')
				validate = false;
			}
		})


		// for email input
		$form.find('input[required][type="checkbox"][name="agree_box"]').each(function () {
			if ($(this).prop('checked') == false) {
				$(this).closest('.form__field--agree').addClass('warning')
				validate = false;
			}
		})

		return validate;
	}



	$('.js_form_btn').click(function (event) {
		event.preventDefault()

		$form = $(this).closest('form')
		$form.find('input[required],textarea[required]').removeClass('warning')

		if (validateForm($form)) {
			// $(this).trigger('submit');
			Fancybox.close();
			Fancybox.show([{
				src: "#js_modal__thanks"
			}])
		}
	});


	$('.js_form input,.js_form textarea').focus(function (e) {
		e.preventDefault();
		$(this).removeClass('warning')
	});


	$('input[required][type="checkbox"][name="agree_box"]').change(function () {
		$(this).closest('.form__field--agree').removeClass('warning')
	})

	let im = new Inputmask("+ 999 (9) 999-9999");
	document.querySelectorAll(".js_phone").forEach((item) => {
		im.mask(item);
	});




	Fancybox.bind('.js__modal', {
		autoFocus: false,
		trapFocus: false,
		closeButton: 'inside',
	});

	$('.js_category__list .category__item__title').click(function (e) {
		if ($(window).width() <= 500) {
			e.preventDefault()
			$(this).toggleClass('active')
			$(this).closest('.category__item').find('.category__item__info__list').stop().slideToggle(400)
		}
	})



	$('.js_home__promo__list .home__promo__location__item').click(function(){
		if ($(this).hasClass('fold')){
			$('.js_home__promo__list .home__promo__location__item.unfold').removeClass('unfold').addClass('fold')
			$(this).removeClass('fold').addClass('unfold')
		}
	})


	$('.js_home__promo__mobile__links a').click(function(e){
		e.preventDefault()
		if (!$(this).hasClass('active')){
			$('.js_home__promo__mobile__links a.active').removeClass('active')
			$(this).addClass('active')
			$('.js_home__promo__list .home__promo__location__item').eq($(this).index()).trigger('click')
		}
	})


	$('.js__btn_close__modal').click(function (e) {
		Fancybox.close();
	});

	$('.js_show_more__property').click(function (e) {
		e.preventDefault()

		let parent = $(this).closest('.js_area_list__page');
		let showCountEl = parseInt(parent.data('show'));
		let count = 0;


		if (parent.find('.area_item.hideMobile').length < showCountEl) {
			$(this).slideUp(400, function () {
				$(this).remove()
			});
		}
		parent.find('.area_item.hideMobile').each(function (index, element) {
			count++;
			if (count == showCountEl) return false;
			$(this).slideDown(400, function () {
				$(this).removeClass('hideMobile')
			});;
		});

	})



	$('.js_layout__block__x .layout__block__tab a').click(function(){
		if ($(this).hasClass('active')) return false;
		$('.js_layout__block__x .layout__block__tab a.active').removeClass('active')
		$(this).addClass('active')
		let ind = $(this).index();
		$(this).closest('.js_layout__block__x').find('.js_layout__block__list .layout__block__item.active').slideUp(400, function () {
			$(this).removeClass('active')
			$(this).closest('.js_layout__block__x').find('.js_layout__block__list .layout__block__item').eq(ind).slideDown(400,function(){
				$(this).addClass('active')
			})
		});

	})


	$(window).scroll(function () {
		if ($(window).scrollTop() > 20) {
			$('.js_header_main').addClass('scrolling');
		} else {
			$('.js_header_main').removeClass('scrolling');
		}

		if ($(window).scrollTop() > 500) {
			$('.js_btn__up').addClass('show');
		} else {
			$('.js_btn__up').removeClass('show');
		}
	})

	$('.js_btn__up').click(function (e) {
		e.preventDefault();
		$("html, body").animate({
			scrollTop: 0
		}, "slow");
	})

	$('.js_btn_show__submenu').click(function (e) {
		e.preventDefault();
		$(this).closest('li').toggleClass('active')
		$(this).closest('li').find('.modal__submenu').stop().slideToggle(400);

	})



	function teamSlider() {

		let slider = $('.js_team__list');
		let sliderParent = slider.closest('.js_slider__wrap');
		let countSlides = slider.find('>').length;

		if (countSlides < 10) countSlides = '0' + countSlides;
		sliderParent.find('.slider_info__number__max').html(countSlides)

		function currenytlider(currrentSlide) {
			let $companyCurrent = sliderParent.find('.slider_info__number__current')
			if (currrentSlide < 10) currrentSlide = '0' + currrentSlide;

			$companyCurrent.html(currrentSlide)
		}

		slider.on('init', function () {
			currenytlider(1)
		});

		slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
			currenytlider(currentSlide + 1)
		});

		slider.slick({
			infinite: true,
			prevArrow: sliderParent.find('.slider__arrow:first-child'),
			nextArrow: sliderParent.find('.slider__arrow:last-child'),
			slidesToShow: 4,
			autoplay: true,
  			autoplaySpeed: 7000,
			speed: 1000,
			swipeToSlide: true,
			responsive: [{
					breakpoint: 1100,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 1,
						variableWidth: true
					}
				},
			]
		})
	}

	function developSlider() {
		let slider = $('.js_slider__developer');
		let sliderParent = slider.closest('.js_slider__wrap');
		let countSlides = slider.find('>').length;

		if (countSlides < 10) countSlides = '0' + countSlides;
		sliderParent.find('.slider_info__number__max').html(countSlides)

		function currenytlider(currrentSlide) {
			let $companyCurrent = sliderParent.find('.slider_info__number__current')
			if (currrentSlide < 10) currrentSlide = '0' + currrentSlide;

			$companyCurrent.html(currrentSlide)
		}

		slider.on('init', function () {
			currenytlider(1)
		});

		slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
			currenytlider(currentSlide + 1)
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').removeClass('active')
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').eq(currentSlide).addClass('active')
		});

		slider.slick({
			infinite: true,
			prevArrow: sliderParent.find('.slider__arrow:first-child'),
			nextArrow: sliderParent.find('.slider__arrow:last-child'),
			slidesToShow: 1,
			swipeToSlide: true,
			autoplay: true,
  			autoplaySpeed: 7000,
			speed: 1000,
		})

		$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').click(function () {
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').removeClass('active')
			$(this).addClass('active')
			slider.slick('slickGoTo', $(this).index());
		})
	}

	function type1Slider(slider) {
		let sliderParent = slider.closest('.js_slider_type_1__wrap');
		let countSlides = slider.find('>').length;

		if (countSlides < 10) countSlides = '0' + countSlides;
		sliderParent.find('.slider_info__number__max').html(countSlides)

		function currenytlider(currrentSlide) {
			let $companyCurrent = sliderParent.find('.slider_info__number__current')
			if (currrentSlide < 10) currrentSlide = '0' + currrentSlide;

			$companyCurrent.html(currrentSlide)
		}

		slider.on('init', function () {
			currenytlider(1)
		});

		slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
			currenytlider(currentSlide + 1)
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').removeClass('active')
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').eq(currentSlide).addClass('active')
		});

		slider.slick({
			infinite: true,
			prevArrow: sliderParent.find('.slider__arrow:first-child'),
			nextArrow: sliderParent.find('.slider__arrow:last-child'),
			slidesToShow: 1,
			swipeToSlide: true,
			fade: true,
			autoplay: true,
  			autoplaySpeed: 7000,
			speed: 1000,
			infinite:false
		})

		$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').click(function () {
			$('.js_page_about__developer__slider__nav .page_about__developer__slider__nav__item').removeClass('active')
			$(this).addClass('active')
			slider.slick('slickGoTo', $(this).index());
		})
	}





	function type1SliderVideo(slider) {
		let sliderParent = slider.closest('.js_home__appartament__video');
		let countSlides = slider.find('>').length;

		if (countSlides < 10) countSlides = '0' + countSlides;
		sliderParent.find('.slider_info__number__max').html(countSlides)

		function currenytlider(currrentSlide) {
			let $companyCurrent = sliderParent.find('.slider_info__number__current')
			if (currrentSlide < 10) currrentSlide = '0' + currrentSlide;

			$companyCurrent.html(currrentSlide)
		}

		slider.on('init', function () {
			currenytlider(1)
		});

		slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
			currenytlider(currentSlide + 1)
			startAnimate()
		});

		slider.slick({
			infinite: true,
			prevArrow: sliderParent.find('.slider__arrow:first-child'),
			nextArrow: sliderParent.find('.slider__arrow:last-child'),
			slidesToShow: 1,
			swipeToSlide: true,
			fade: true,
			autoplay: true,
  			autoplaySpeed: 7000,
			speed: 1000,
			infinite:false
		})

		function startAnimate(){
			$('.js_home__appartament__video__item_x__line').removeClass('active')
			slider.find('.slick-current .js_home__appartament__video__item_x__line').addClass('active')
		}

		slider.find('.slick-slide').hover(function () {
			$('.js_home__appartament__video__item_x__line').removeClass('active')
			}, function () {
				slider.find('.slick-current .js_home__appartament__video__item_x__line').addClass('active')
			}
		);
	}








	$('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
		e.preventDefault();
		$('.js_btn_menu').toggleClass('active')
		$('.js__modal__menu').toggleClass('active')
		$('.js_modal__menu__overlay').toggleClass('active')
	});



	$('body').click(function (event) {
		if (!$(event.target).closest('.js_filter__block__price').length && $('.js_filter__block__price').hasClass('active')){
			$('.js_filter__block__price').removeClass('active')
		}
		if (!$(event.target).closest('.js_archive__top__info__order').length && $('.js_archive__top__info__order').hasClass('active')){
			$('.js_archive__top__info__order').toggleClass('active')
		}
		if (!$(event.target).closest('.js_page__area__top__slider').length && $('.js_page__area__top__slider').hasClass('active')){
			$('.js_page__area__top__slider').toggleClass('active')
		}
	})

	$('.js_filter__block__price .filter__block__price__view').click(function () {
		$(this).closest('.js_filter__block__price').toggleClass('active')
	})

	$('.js_archive__top__info__order').click(function () {
		$(this).toggleClass('active')
	})

	$('.js_page__area__top__slider').click(function () {
		$(this).toggleClass('active')
	})




	if ($('.js_home__appartament__video__list').length) {
		$('.js_home__appartament__video__list').each(function(){
			type1SliderVideo($(this))
		})
	}


	if ($('.js_slider_type_1').length) {
		$('.js_slider_type_1').each(function(){
			type1Slider($(this))
		})
	}
	if ($('.js_team__list').length) teamSlider()
	if ($('.js_slider__developer').length) developSlider()

});